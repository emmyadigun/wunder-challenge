import React, { Component } from 'react';
import './App.scss';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import NavBar from './components/CustomNavbar';
import MainForm from './components/MainForm';
import SuccessView from './components/steps/SuccessView'

class App extends Component {

  render() {
    return (
        <div>

      <Router>
        <div>
        <NavBar />
          <Route exact path="/" component={MainForm} />
          <Route exact path="/SuccessView" component={SuccessView} />

          <div className="footer">
          <p>© Wunder Mobility 2019</p>
        </div>
        </div>
      </Router>
      </div>
    );
  }
}

export default App;
