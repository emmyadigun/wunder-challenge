import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class CustomNavbar extends Component{
  render(){
    return(
      <nav className="navbar navbar-default navbar-fixed-top">
      <div className="container">

        <div className="navbar-header">
          <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span className="sr-only">Toggle navigation</span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
          </button>
          <Link className="navbar-brand" to="/"><img src="assets/images/Wunder-Mobility-Black.png" width="200" alt={"Paypoint logo"} /></Link>
        </div>
        <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul className="nav navbar-nav navbar-right">
            <li><button className="btn btn-contact navbar-btn"><Link to="">Contact</Link></button></li>
          </ul>
        </div>
      </div>
    </nav>

    )
  }
}
