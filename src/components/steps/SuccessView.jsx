import React, {Component} from 'react'

class SuccessView extends Component{
  state = {
    setData : []
  }

  componentWillMount(){
    this.setState({
          setData:localStorage.getItem("userData")
        })
      }
      
  render(){
    const {setData} = this.state
    return(
      <div>
      <div className="cont-style">
      <h3 className="text-center success-pad"><strong>Your data has been saved!</strong></h3>
      <p className="text-center"><strong>Your Payment Data Id is </strong></p>
      <h5 className="data-style"> {setData}</h5>
      </div>
      </div>


    )
  }
}

export default SuccessView
