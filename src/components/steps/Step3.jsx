import React, {Component} from 'react'

class Step3 extends Component{
  render(){

    //If current step is not 3 don't render
    if(this.props.currentStep !==3){
      return null
    }

    //This gets the values for the input fields
    const {values} = this.props;
    const {successMessage} = this.props;

    return(

      <div>
      <h4 className="text-center"><strong>Payment Information</strong></h4>
      <p>{successMessage}</p>
      <div className="form-group">
      <label><i className="fa fa-user"></i> Account Owner</label>
      <input
      type="text"
      className="form-control"
      placeholder="Account Owner"
      name="owner"
      noValidate
      value={values.owner}
      onChange={this.props.handleChange('owner')}
      />
      </div>

      <div className="form-group">
      <label><i className="fa fa-credit-card"></i> IBAN</label>
      <input
      type="text"
      className="form-control"
      placeholder="IBAN"
      name="iban"
      noValidate
      value={values.iban}
      onChange={this.props.handleChange('iban')}
      />
      </div>

        <button className="btn btn-success pull-right" onClick={this.handleSubmit}>Submit</button>
      </div>

    )
  }
}

export default Step3
