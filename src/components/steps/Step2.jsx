import React, {Component} from 'react'

class Step2 extends Component{
  render(){

//If current step is not 2 don't render
    if(this.props.currentStep !==2){
      return null
    }

    //This gets the values for the input fields
    const {values} = this.props;

    return(

      <div>
      <h4 className="text-center"><strong>Address Information</strong></h4>
      <div className="form-group">
      <label><i className="fa fa-subway"></i> Street</label>
      <input
      type="text"
      className="form-control"
      placeholder="Street"
      name="streetName"
      noValidate
      value={values.streetName}
      onChange={this.props.handleChange('streetName')}
      />
      </div>

      <div className="form-group">
      <label><i className="fa fa-home"></i> House Number</label>
      <input
      type="number"
      className="form-control"
      placeholder="House Number"
      name="houseNumber"
      noValidate
      value={values.houseNumber}
      onChange={this.props.handleChange('houseNumber')}
      />
      </div>

      <div className="form-group">
      <label><i className="fa fa-fax"></i> Zip Code</label>
      <input
      type="number"
      className="form-control"
      placeholder="Zip Code"
      name="zipCode"
      noValidate
      value={values.zipCode}
      onChange={this.props.handleChange('zipCode')}
      />
      </div>

      <div className="form-group">
      <label><i className="fa fa-building-o"></i> City</label>
      <input
      type="text"
      className="form-control"
      placeholder="City"
      name="cityName"
      noValidate
      value={values.cityName}
      onChange={this.props.handleChange('cityName')}
      />
      </div>

      </div>

    )
  }
}

export default Step2
