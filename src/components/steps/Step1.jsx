import React, { Component } from 'react'

class Step1 extends Component{
  render(){

//If current step is not 1 don't render
    if(this.props.currentStep !==1){
      return null
    }

//This gets the values for the input fields
    const { values } = this.props;
    
    return(

      <div>
      <h4 className="text-center"><strong>Personal Information</strong></h4>
      <div className="form-group">
      <label><i className="fa fa-user"></i> Full Name</label>
      <input
      type="text"
      className="form-control"
      placeholder="First name"
      name="firstName"
      noValidate
      value={values.firstName}
      onChange={this.props.handleChange('firstName')}
      required
      />
      </div>

      <div className="form-group">
      <label><i className="fa fa-user"></i> Last Name</label>
      <input
      type="text"
      className="form-control"
      placeholder="Last Name"
      name="lastName"
      noValidate
      value={values.lastName}
      onChange={this.props.handleChange('lastName')}
      required
      />
      </div>

      <div className="form-group">
      <label><i className="fa fa-phone"></i> Phone Number</label>
      <input
      type="number"
      className="form-control"
      placeholder="Phone Number"
      name="phone"
      noValidate
      value={values.phone}
      onChange={this.props.handleChange('phone')}
      required
      />
      </div>

</div>

    )
  }
}

export default Step1
