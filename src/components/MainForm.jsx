import React, {Component} from 'react'
import Step1 from '../components/steps/Step1'
import Step2 from '../components/steps/Step2'
import Step3 from '../components/steps/Step3'
import SuccessView from '../components/steps/SuccessView'
import axios from 'axios'
import firebase from '../firebase.js'


class MainForm extends Component{
  constructor(props) {
   super(props);

   if(localStorage.getItem('message')) {
     this.state = JSON.parse(localStorage.getItem('message')); //in this case the state is just for input values
   } else {
     this.state = {
       currentStep:1,
       firstName: '',
       lastName: '',
       phone: '',
       streetName: '',
       houseNumber: '',
       zipCode: '',
       cityName: '',
       owner: '',
       iban:''
     }
   }
 }
 //get form values and save in local storage
    handleChange = input => (e) =>{
      this.setState({ [input] : e.target.value }, () => {
      localStorage.setItem('message', JSON.stringify(this.state));
    });

    }

//post form data(customerID Account Number(IBAN) and Account Name) to API to get paymentDataId and save
// data to firebase
    handleSubmit = async (e) =>{
      e.preventDefault()

      const itemsRef = firebase.database().ref('details');

      let headers = {
        'Content-Type': 'application/json'

        }
        let previousId = 0
        let mData = JSON.stringify({
        customerId : previousId + 1,
        iban:this.state.iban,
        owner:this.state.owner

      });
      console.log(mData)
//API call to get paymentDataId. Headers was not passed because of CORS issue. This needs to resolved from the server
//or install CORS plugin
      axios({ url: 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data', data: mData, headers:headers, method: 'post'})
        .then((response) => {
          if(response.data.paymentDataId !== null){
            const item = {
              FirstName: this.state.firstName,
              LastName: this.state.lastName,
              Telephone: this.state.phone,
              Street: this.state.streetName,
              House: this.state.houseNumber,
              ZipCode: this.state.zipCode,
              City: this.state.cityName,
              owner: this.state.owner,
              iban: this.state.iban,
              paymentDataId: response.data.paymentDataId
            }
              itemsRef.push(item);
            //alert("Data Successfully Submitted" + '\n\n' + ' Payment Data Id ' + '\n\n' + response.data.paymentDataId)
              localStorage.setItem('userData', response.data.paymentDataId);
              console.log(response.data.paymentDataId)
              window.location.href = '/SuccessView'; //redirects to Success View to see paymentDataId
          }
          return
        })
          .catch((error) => {
            alert(error.message)
            console.log(error)
          })
      }

//form wizard logic for next step
    _next = ()=>{
      let currentStep = this.state.currentStep
      currentStep = currentStep >= 2? 3: currentStep + 1
      this.setState({
        currentStep : currentStep
      })
    }

//form wizard logic for previous step
    _prev = ()=>{
      let currentStep = this.state.currentStep
      currentStep = currentStep <= 1? 1: currentStep - 1
      this.setState({
        currentStep : currentStep
      })
    }

//previous button is displayed
     previousButton(){
      let currentStep = this.state.currentStep;

      if(currentStep !== 1){
        return(
          <button className="btn btn-secondary" type="button" onClick={this._prev}>
            Previous
          </button>
        )
      }
      return null;
    }

//next button is displayed
     nextButton(){
      let currentStep = this.state.currentStep;

      if(currentStep < 3){
        return(
          <button className="btn btn-primary pull-right" type="button" onClick={this._next}>
            Next
          </button>
        )
      }
      return null;
    }

  render(){
//render the states of form data of the 3 steps
    const { firstName, lastName, phone, streetName, houseNumber, zipCode, cityName, owner, iban } = this.state;
    const values = { firstName, lastName, phone, streetName, houseNumber, zipCode, cityName, owner, iban };

    return(
      <div>
      <div className="container">
      <div className="steps-style">
          <form onSubmit={this.handleSubmit} noValidate>
            <Step1
            currentStep={this.state.currentStep}
            values = {values}
            handleChange = {this.handleChange}
            />

            <Step2
            currentStep={this.state.currentStep}
            values = {values}
            handleChange = {this.handleChange}
            />

            <Step3
            currentStep={this.state.currentStep}
            values = {values}
            handleChange = {this.handleChange}

            />

            {this.previousButton()}
            {this.nextButton()}

          </form>
      </div>
      </div>
      </div>



    )
  }
}

export default MainForm
