import firebase from 'firebase'

//This data had to be displayed because of the challenge and also doing a test. Ideally
//Environment variables should be used.
const firebaseConfig = {
  apiKey: "AIzaSyDqGFzvhre2Qt4hNvgzIbrXuzcjZCV094I",
    authDomain: "test-wunder.firebaseapp.com",
    databaseURL: "https://test-wunder.firebaseio.com",
    projectId: "test-wunder",
    storageBucket: "test-wunder.appspot.com",
    messagingSenderId: "729361316946",
    appId: "1:729361316946:web:1eb7443f8d207df8"
};

const fire = firebase.initializeApp(firebaseConfig);
export default fire;
