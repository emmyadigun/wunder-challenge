Project Name: The web application is a basic user registration.
This project was built with react and can be viewed here (https://wunder-challenge.herokuapp.com/).

## Possible Optimization for my code

Avoid the use of cdn to load external/third party library styles and scripts. This will make the system consume less memory and execute faster


## What could have been done better than I have done it

 - The use of notification library to display success and error message
 - Verification of customer data saved in the database to avoid multiple customer entry,
 - The Payment Data Id shows only when CORS is enabled from the server or using a CORS enabling plugin

## Code structure

I made use of react frame work which supports Single Page Application and helps organise the UI in a component
structure. Each of the steps have their individual components. I also made use of Firebase to save customer data
including the paymentDataId.

## Technologies Used

- React - Front-end development,  
- Firebase - To save customer details including the Payment Data Id,  
- SCSS - SASS,
- Style Sheet - Bootstrap  
- A sample of the saved data in firebase can be viewed here (https://drive.google.com/file/d/1oeD-p_rzd4VTmhbyXo2tcO_5IzBTmgau/view?usp=drive_web)

## Challenges

CORS issue -  I had to install Moesif Origins & CORS Changer plugin on my browser to get paymentDataId.
